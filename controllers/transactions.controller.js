const {validateFunction, validateUpdateFunction, Transactions} = require('../models/transactions.model');

// Create and Save a new Transactions
exports.create = (req, res) => {
    // Validate request
    let params = Object.assign({}, req.body, req.params, req.query);
    // Validate request
    let validation = validateFunction(params);

    if(validation.length) {
        return res.status(400).send(validation);
    }

    // Create a Transactions
    const transaction = new Transactions(params);

    // Save Transactions in the database
    transaction.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Transactions."
        });
    });
};

// Retrieve and return all data from the database.
exports.findAll = (req, res) => {
    Transactions.find().then(data => {
        if(data.length > 0){
            res.send(data);
        }else{
            res.status(204).send({
                message: "no data found."
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving data."
        });
    });
};

// Find a single transaction with a transactionId
exports.findOne = (req, res) => {
    console.log("transaction.findOne", req.params.transactionId)

    Transactions.findById(req.params.transactionId)
    .then(transaction => {
        if(!transaction) {
            return res.status(204).send({
                message: "Transactions not found with id " + req.params.transactionId
            });            
        }
        res.send(transaction);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(204).send({
                message: "no transaction found with id " + req.params.transactionId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving transaction with id " + req.params.transactionId
        });
    });
};

// Update a transaction identified by the transactionId in the request
exports.update = (req, res) => {

    console.log("product.update", req.params.transactionId)

    let params = Object.assign({}, req.body, req.params, req.query);

    // Validate request
    let validation = validateUpdateFunction(params);
    if(validation.length) {
        return res.status(400).send(validation);
    }

    // Find transaction and update it with the request body
    Transactions.findByIdAndUpdate(req.params.transactionId, params, {new: true})
    .then(transaction => {
        if(!transaction) {
            return res.status(204).send({
                message: "Transactions not found with id " + req.params.transactionId
            });
        }
        res.send(transaction);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(204).send({
                message: "Transactions not found with id " + req.params.transactionId
            });                
        }
        return res.status(500).send({
            message: "Error updating transaction with id " + req.params.transactionId
        });
    });
};