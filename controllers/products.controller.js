const {validateFunction, validateUpdateFunction, Products} = require('../models/products.model');

// Create and Save a new Products
exports.create = (req, res) => {

    let params = Object.assign({}, req.body, req.params, req.query);
    // Validate request
    let validation = validateFunction(params);
    if(validation.length) {
        return res.status(400).send(validation);
    }

    // Create a Products
    const product = new Products(params);

    // Save Products in the database
    product.save().then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Products."
        });
    });
};

// Retrieve and return all data from the database.
exports.findAll = (req, res) => {
    Products.find().then(data => {
        if(data.length > 0){
            res.send(data);
        }else{
            res.status(204).send({
                message: "no data found."
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving data."
        });
    });
};

// Find a single product with a productId
exports.findOne = (req, res) => {

    console.log("product.findOne", req.params.productId)

    Products.findById(req.params.productId)
    .then(product => {
        if(!product) {
            return res.status(204).send({
                message: "Products not found with id " + req.params.productId
            });            
        }
        res.send(product);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(204).send({
                message: "no product found with id " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving product with id " + req.params.productId
        });
    });
};

// Update a product identified by the productId in the request
exports.update = (req, res) => {

    console.log("product.update", req.params.productId)

    let params = Object.assign({}, req.body, req.params, req.query);

    // Validate request
    let validation = validateUpdateFunction(params);
    if(validation.length) {
        return res.status(400).send(validation);
    }

    // Find product and update it with the request body
    Products.findByIdAndUpdate(req.params.productId, params, {new: true})
    .then(product => {
        if(!product) {
            return res.status(204).send({
                message: "Products not found with id " + req.params.productId
            });
        }
        res.send(product);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(204).send({
                message: "Products not found with id " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Error updating product with id " + req.params.productId
        });
    });
};

// Delete a product with the specified productId in the request
exports.delete = (req, res) => {
    Products.findOneAndDelete(req.params.productId)
    .then(product => {
        if(!product) {
            return res.status(204).send({
                message: "Products not found with id " + req.params.productId
            });
        }
        res.send({message: "Products deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(204).send({
                message: "Products not found with id " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Could not delete product with id " + req.params.productId
        });
    });
};
