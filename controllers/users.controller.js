const {validateFunction, validateUpdateFunction, Users} = require('../models/users.model');

// Create and Save a new Users
exports.create = (req, res) => {
    // Validate request
    let params = Object.assign({}, req.body, req.params, req.query);
    // Validate request
    let validation = validateFunction(params);

    if(validation.length) {
        return res.status(400).send(validation);
    }
    // Create a Users
    const user = new Users(params);

    // Save Users in the database
    user.save().then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Users."
        });
    });
};

// Retrieve and return all data from the database.
exports.findAll = (req, res) => {
    Users.find().then(data => {
        if(data.length > 0){
            res.send(data);
        }else{
            res.status(204).send({
                message: "no data found."
            });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving data."
        });
    });
};

// Find a single user with a userId
exports.findOne = (req, res) => {

    console.log("users.findOne", req.params.userId)

    Users.findById(req.params.userId)
    .then(user => {
        if(!user) {
            return res.status(204).send({
                message: "Users not found with id " + req.params.userId
            });            
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(204).send({
                message: "no user found with id " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params.userId
        });
    });
};

// Update a user identified by the userId in the request
exports.update = (req, res) => {

    console.log("product.update", req.params.userId)

    let params = Object.assign({}, req.body, req.params, req.query);

    // Validate request
    let validation = validateUpdateFunction(params);
    if(validation.length) {
        return res.status(400).send(validation);
    }

    // Find user and update it with the request body
    Users.findByIdAndUpdate(req.params.userId, params, {new: true})
    .then(user => {
        if(!user) {
            return res.status(204).send({
                message: "Users not found with id " + req.params.userId
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(204).send({
                message: "Users not found with id " + req.params.userId
            });                
        }
        return res.status(500).send({
            message: "Error updating user with id " + req.params.userId
        });
    });
};