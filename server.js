'use strict';

const express = require('express');
const { v4: uuidv4 } = require('uuid');
// const bodyParser = require('body-parser');
const productRoutes = require("./routes/products.routes");
const userRoutes = require("./routes/users.routes");
const transRoutes = require("./routes/transactions.routes");

const dbConn = require("./dbConnect")
// create express app
const app = express();

app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

/**
 * request logging
 */
 app.use(function (req, res, next) {
  var reqId = uuidv4();
  req.body.reqId = reqId;
  req.body.userAgent = req.headers['user-agent'];
  var loggedInStatus = (req.query && req.query.userId && req.query.userId != 'Guest') ? "loggedIn" : "Guest";
  req.query.loggedInStatus = loggedInStatus;
  console.log("server", reqId, {
    req: req.method + " " + req.originalUrl,
    req_params: req.params || "",
    req_body: req.body || "",
    loggedInStatus: loggedInStatus
  });
  next();
});

// define a simple route
app.get('/', (req, res) => {
  res.json({"message": "success"});
});

app.use('/products', productRoutes)
app.use('/users', userRoutes)
app.use('/account', transRoutes)

//require('./routes/products.routes')(app);

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
  
// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') == 'dev' ? err : err;

    // render the error page
    res.status(err.status) ;
    res.send(err);
});


  /**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
}
  
  /**
   * Event listener for HTTP server "error" event.
   */
  function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }
  
    var bind = typeof port === 'string' ?
      'Pipe ' + port :
      'Port ' + port;
  
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }


var port = normalizePort('8080');
app.set('port', port);

// listen for requests
app.listen(port, () => {
    console.log("Server is listening on port -- " + port + " -env: " + process.env.NODE_ENV);
}).on('error', onError);