const mongoose = require('mongoose');
const util = require("../util/util")


const jsonSchema = {
    properties: {
        id: {type: String},
        username: {type: String},
        addresses: [{
            coords: {type: String},
            label: {type: String},
            mapRegion: { 
                latitude: {type: Number},
                longitude: {type: Number},
                latitudeDelta: {type: Number},
                longitudeDelta: {type: Number}
            },
            isCorrentSelected: {type: Boolean},
            address: {type: String},
            building: {type: String},
            buttonVisible: {type: Boolean},
            buttonColor: {type: String},
            selectedlabel: {type: Number}
        }],
        paymentMethods: [{
            name: {type: String},
            type: {type: String}
        }],
        activities: [{
            activityName: {type: String},
            activityType: {type: String}
        }]
    },
    required: ["username", "id"]
};

const userSchema = mongoose.Schema(jsonSchema.properties);

module.exports.validateFunction = (params) => {
    return util.validateFunction(params, jsonSchema)
}

module.exports.validateUpdateFunction = (params) => {
    return util.validateUpdateFunction(params, jsonSchema)
}

module.exports.Users = mongoose.model('Users', userSchema);