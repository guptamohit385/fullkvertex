const mongoose = require('mongoose');
const util = require("../util/util")

const jsonSchema = {
    properties: {
        totalAmount: {type: Number},
        deliveryAddress: {type: mongoose.Types.ObjectId},
        billingAddress: {type: mongoose.Types.ObjectId},
        paymentMethods: {
            name: {type: String},
            type: {type: String}
        },
        orders: [{
            productId: {type: mongoose.Types.ObjectId},
            quantity: {type: Number}
        }],
        subscriptionStatus: {type: String}, // if subscription activie or inactive
        paymentStatus: {type: String}, // success or failed or pending
        paymentType: {type: String}, // one time or subscription
        paymentSchedule: {type: String}, // if subscription monthly, quaterly, yearly
        paymentScheduleDate: {type: String}, // if subscription dd-mm-yyyy
        paymentSchedulesSubsId: {type: mongoose.Types.ObjectId},
        orderDate: {type: String},
        lastUpdate: {type: String},
        DeliveredOn: {type: String},
        ExpectedOn: {type: String},
        discount: {type: Number},
        couponcode: {type: Number}
        // if subscription transaction id
    },
    required: ["totalAmount", "delivaryAddress", "billingAddress", "paymentMethods", "orders"]
}

const transactionSchema = mongoose.Schema(jsonSchema.properties);

module.exports.validateFunction = (params) => {
    return util.validateFunction(params, jsonSchema)
}

module.exports.validateUpdateFunction = (params) => {
    return util.validateUpdateFunction(params, jsonSchema)
}

module.exports.Transactions = mongoose.model('Transactions', transactionSchema);