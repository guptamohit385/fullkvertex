const mongoose = require('mongoose');
const util = require("../util/util")

const jsonSchema = {
    properties: {
        category: {type: String},
        subCategory: {type: String},
        productName: {type: String},
        likes: {type: Number},
        ourPrice: {type: Number},
        currency: {type: String},
        discount: {type: Number},
        marketPrice: {type: String},
        ImageUrl: {type: String},
        productVender: {type: String},
        rating: {type: Number},
        desciption: {type: String},
        galary: [{type: String}],
        features: [{type: String}],
        packing: [{
            quantity: {type: Number},
            unit: {type: String},
            available: {type: Boolean},
            price: {type: Number},
            currency: {type: String},
            color: {type: String}
        }]
    },
    required: ["category", "subCategory", "productName", "ourPrice", "currency", "ImageUrl", "features", "packing" ]
}

const productSchema = mongoose.Schema(jsonSchema.properties);

module.exports.validateFunction = (params) => {
    return util.validateFunction(params, jsonSchema)
}

module.exports.validateUpdateFunction = (params) => {
    return util.validateUpdateFunction(params, jsonSchema)
}

module.exports.Products = mongoose.model('Products', productSchema);
