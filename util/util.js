const Validator = require('jsonschema').Validator;
const v = new Validator();

module.exports.validateFunction = (params, jsonSchema) => {
    let validRes = v.validate(params, jsonSchema).errors;

    if(!validRes.length) {
      return []
    }
    else {
      return validRes
    }
}

module.exports.validateUpdateFunction = (params, jsonSchema) => {
    let validRes = v.validate(params, {properties: jsonSchema.properties}).errors;

    if(!validRes.length) {
      return []
    }
    else {
      return validRes
    }
}