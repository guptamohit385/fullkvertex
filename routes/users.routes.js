let express = require('express');
let app = express.Router();

const users = require('../controllers/users.controller');

// Create a new User
app.post('/', users.create);

// Retrieve all User
app.get('/', users.findAll);

// Retrieve a single User with UserId
app.get('/:userId', users.findOne);

// Update a User with UserId
app.put('/:userId', users.update);


module.exports = app;