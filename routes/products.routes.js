let express = require('express');
let app = express.Router();

const products = require('../controllers/products.controller');

// Create a new Note
app.post('/', products.create);

// Retrieve all Notes
app.get('/', products.findAll);

// Retrieve a single Note with noteId
app.get('/:productId', products.findOne);

// Update a Note with noteId
app.put('/:productId', products.update);

// Delete a Note with noteId
app.delete('/:productId', products.delete);


module.exports = app;