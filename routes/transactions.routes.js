let express = require('express');
let app = express.Router();

const transactions = require('../controllers/transactions.controller');

// Create a new Note
app.post('/', transactions.create);

// Retrieve all Notes
app.get('/', transactions.findAll);

// Retrieve a single Note with noteId
app.get('/:transactionsId', transactions.findOne);

// Update a Note with noteId
app.put('/:transactionsId', transactions.update);


module.exports = app;